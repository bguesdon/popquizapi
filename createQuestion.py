import json
import sys
import requests

# url = 'http://localhost:3000/api/question'
url = 'https://popquizapi.herokuapp.com/api/question'
with open(sys.argv[1]) as f:
  data = json.load(f)

i = 1

for x in data['quizz']['fr']['débutant']:
    myobj = {'phrase': x['question'], 'answer': x['réponse'], 'image': ''}
    answ = requests.post(url, json = myobj)
    print(answ)
    i += 1
