const bcrypt = require('bcrypt');
const User = require('../models/user');
const jwt = require("jsonwebtoken");

const {google} = require('googleapis');
const request = require('request');

const oauth2Client = new google.auth.OAuth2(
    "1034367477674-rqcqhdocq4caq8mn0gnfkmr5pth2hptj.apps.googleusercontent.com",
    "7rnQZiRU0o_r0XKuNdn4GRxN",
    "http://localhost:8080"
);

exports.login = (req, res, next) => {
    User.findOne({ mail: req.body.mail }).select('+password')
        .then(user => {
            if (!user) {
                return res.status(401).json({error: 'Unauthorized'});
            }
            if (req.body.password !== user.password) {
                return res.status(401).json({error: 'Unauthorized'});
            }
            res.status(200).json({
                userId: user._id,
                token: jwt.sign(
                    {userId: user._id},
                    'TOKEN_SECRET',
                    {expiresIn: '24h'}
                )
            });
        });
};

exports.register = (req, res, next) => {
    User.findOne({ mail: req.body.mail })
        .then(user => {
            if (!user) {
                const newUser = new User({
                    name: req.body.name,
                    mail: req.body.mail,
                    password: req.body.password,
                    friendsIds: req.body.friendsIds,
                    friendReqs: req.body.friendReqs,
                    roomId: req.body.roomId,
                });
                newUser.save().then(
                    () => {
                         return res.status(201).json({
                            message: 'User registered succesfully',
                            userId: newUser._id
                        });
                    }
                ).catch(
                    (error) => {
                        res.status(400).json({
                            error: error
                        });
                    }
                );
            }
            if (user) {
                return res.status(409).json({error: 'User already exist'});
            }
        });
};

exports.googleCallback = (req, res, next) => {
    console.log(req.body.code)
    oauth2Client.getToken(req.body.code).then(token => {
        const options = {
            url: 'https://www.googleapis.com/oauth2/v2/userinfo',
            headers: {
                'Authorization': 'Bearer ' + token.tokens['access_token']
            }
        };
        request(options, function (err, response, body) {
            if (!err && response.statusCode === 200) {
                const info = JSON.parse(body);
                console.log(info);
                User.findOne({ mail: info.email })
                    .then(user => {
                        if (!user) {
                            const newUser = new User({
                                name: info.given_name,
                                mail: info.email,
                                password: info.id,
                                friendsIds: [],
                                friendReqs: [],
                                roomId: "",
                            });
                            newUser.save();
                             return res.status(200).json({
                                userId: newUser._id,
                                token: jwt.sign(
                                    {userId: newUser._id},
                                    'TOKEN_SECRET',
                                    {expiresIn: '24h'}
                                )
                            });
                        }
                        res.status(200).json({
                            userId: user._id,
                            token: jwt.sign(
                                {userId: user._id},
                                'TOKEN_SECRET',
                                {expiresIn: '24h'}
                            )
                        });
                    });
            }
        });
    }).catch((error) => {
        console.log(error)
    })
};

exports.facebookCallback = (req, res, next) => {
    console.log(req.body);
    User.findOne({mail: req.body.email})
        .then(user => {
            if (!user) {
                const newUser = new User({
                    name: req.body.name,
                    mail: req.body.email,
                    password: req.body.id,
                    friendsIds: [],
                    friendReqs: [],
                    roomId: "",
                });
                newUser.save();
                return res.status(200).json({
                    userId: newUser._id,
                    token: jwt.sign(
                        {userId: newUser._id},
                        'TOKEN_SECRET',
                        {expiresIn: '24h'}
                    )
                });
            }
            res.status(200).json({
                userId: user._id,
                token: jwt.sign(
                    {userId: user._id},
                    'TOKEN_SECRET',
                    {expiresIn: '24h'}
                )
            });
        });
}
