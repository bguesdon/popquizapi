const Question = require('../models/question');
const Answer = require('../models/answer')

exports.createQuestion = (req, res, next) => {
    Question.count()
        .then((cnt) => {
            const question = new Question({
                phrase: req.body.phrase,
                image: req.body.image,
                answerId: "tempo",
                nb: cnt + 1
            });
            const answer = new Answer({
                phrase: req.body.answer,
                questionId: question._id,
                nb: cnt + 1
            });
            question.answerId = answer._id;
            question.save()
                .then(() => {
                    answer.save()
                        .then(() => {
                            res.status(201).json({
                                message: 'Question saved successfully!',
                            });
                        })
                        .catch(
                            (error) => {
                                res.status(400).json({
                                    error: error
                                });
                            }
                        );
                })
                .catch((error) => {
                        res.status(400).json({
                            error: error
                        });
                    }
                );
        })
};

exports.getQuestion = (req, res, next) => {
    Question.findOne({
        _id: req.params.id
    }).then(
        (question) => {
            res.status(200).json(question);
        }
    ).catch(
        (error) => {
            res.status(404).json({
                error: error
            });
        }
    );
};

exports.modifyQuestion = (req, res, next) => {
    Question.updateOne({_id: req.params.id}, {...req.body, _id: req.params.id}).then(
        () => {
            res.status(201).json({
                message: 'Question updated successfully!'
            });
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};

exports.deleteQuestion = (req, res, next) => {
    Question.findOne({_id: req.params.id})
        .then((quest) => {
            Answer.deleteOne({_id: quest.answerId})
                .then(() => {
                    Question.deleteOne({_id: req.params.id}).then(
                        () => {
                            res.status(200).json({
                                message: 'Deleted!'
                            });
                        }
                    ).catch(
                        (error) => {
                            res.status(400).json({
                                error: error
                            });
                        }
                    );
                })
                .catch(
                    (error) => {
                        res.status(400).json({
                            error: error
                        });
                    }
                );
        })
        .catch(
            (error) => {
                res.status(400).json({
                    error: error
                });
            }
        );
};

exports.getAllQuestions = (req, res, next) => {
    Question.find().then(
        (questions) => {
            res.status(200).json(questions);
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};


exports.deleteAllQuestions = (req, res, next) => {
    Question.remove().then(
        () => {
            Answer.remove().then(
                () => {
                    res.status(200).json({
                        message: 'Deleted!'
                    });
                }
            ).catch(
                (error) => {
                    res.status(400).json({
                        error: error
                    });
                }
            );
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
}
