const Friend = require('../models/friend');

exports.createFriend = (req, res, next) => {
    const friend = new Friend({
        userId: req.body.userId
    });
    friend.save().then(
        () => {
            res.status(201).json({
                message: 'Friend saved successfully',
            });
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};

exports.getFriend = (req, res, next) => {
    Friend.findOne({
        _id: req.params.id
    }).then(
        (friend) => {
            res.status(200).json(friend);
        }
    ).catch(
        (error) => {
            res.status(404).json({
                error: error
            });
        }
    );
};

exports.modifyFriend = (req, res, next) => {
    Friend.updateOne({_id: req.params.id}, {...req.body, _id: req.params.id}).then(
        () => {
            res.status(201).json({
                message: 'Friend updated successfully!'
            });
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};

exports.deleteFriend = (req, res, next) => {
    Friend.deleteOne({_id: req.params.id}).then(
        () => {
            res.status(200).json({
                message: 'Deleted!'
            });
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};

exports.getAllFriends = (req, res, next) => {
    Friend.find().then(
        (friends) => {
            res.status(200).json(friends);
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};

exports.getAllFriendsNames = (req, res, next) => {
    Friend.find().then(
        (friends) => {
            res.status(200).json(friends.name);
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};

exports.deleteAllFriends = (req, res, next) => {
    Friend.remove().then(
        () => {
            res.status(200).json({
                message: 'Deleted!'
            });
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
}
