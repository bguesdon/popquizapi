const User = require('../models/user');
const FriendRequest = require('../models/friendRequest');
const RoomRequest = require('../models/roomRequest');
const Room = require('../models/room')

exports.createUser = (req, res, next) => {
    const user = new User({
        name: req.body.name,
        mail: req.body.mail,
        password: req.body.password,
        friendsIds: req.body.friendsIds,
        friendReqs: req.body.friendReqs,
        roomId: req.body.roomId,
        points: 0
    });
    user.save().then(
        () => {
            res.status(201).json({
                message: 'User created succesfully',
                userId: user._id
            });
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};

exports.checkToken = (req, res, next) => {
    res.status(200).json({message: 'success'});
}

exports.getUser = (req, res, next) => {
    User.findOne({
        _id: req.params.id
    }).then(
        (user) => {
            res.status(200).json(user);
        }
    ).catch(
        (error) => {
            res.status(404).json({
                error: error
            });
        }
    );
};

exports.getPoints = (req, res, next) => {
    User.findOne({
        _id: req.params.id
    }).then(
        (user) => {
            res.status(200).json(user.points);
        }
    ).catch(
        (error) => {
            res.status(404).json({
                error: error
            });
        }
    );
};

exports.getFriends = (req, res, next) => {
    User.findOne({
        _id: req.params.id
    }).then(
        (user) => {
            res.status(200).json(user.friendsIds);
        }
    ).catch(
        (error) => {
            res.status(404).json({
                error: error
            });
        }
    );
};

exports.getFriendReqs = (req, res, next) => {
    User.findOne({
        _id: req.params.id
    }).then(
        (user) => {
            res.status(200).json(user.friendReqs);
        }
    ).catch(
        (error) => {
            res.status(404).json({
                error: error
            });
        }
    );
};

exports.modifyUser = (req, res, next) => {
    User.findOne({
        _id: req.params.id
    }).select('+password').then(
        (user) => {
            if (req.body.oldPassword !== user.password) {
                return res.status(401).json({message: 'Invalid password'})
            } else {
                User.updateOne({_id: req.params.id}, {...req.body, _id: req.params.id}).then(
                    () => {
                        res.status(201).json({
                            message: 'User updated successfully!'
                        });
                    }
                ).catch(
                    (error) => {
                        res.status(400).json({
                            error: error
                        });
                    }
                );
            }
        }
    )
};

exports.addFriend = (req, res, next) => {
    User.updateOne({_id: req.params.id}, {$push: {friendsIds: req.body.friendId}}
    ).then(
        User.updateOne({_id: req.body.friendId}, {$push: {friendsIds: req.params.id}}
        ).then(
            () => {
                res.status(201).json({
                    message: 'Friend added successfully!'
                });
            }
        ).catch(
            (error) => {
                res.status(400).json({
                    error: error
                });
            }
        )
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};

exports.addFriendReq = (req, res, next) => {
    const datetime = new Date();
    const friendReq = new FriendRequest({
        friendId: req.body.friendId,
        date: datetime
    })
    User.updateOne({_id: req.params.id}, {$push: {friendReqs: friendReq}}
    ).then(
        () => {
            res.status(201).json({
                message: 'Friend request added successfully!',
                requestId: friendReq.id
            });
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};

function addUserToRoom(roomId, userId, res) {
    Room.updateOne({_id: roomId},
        {$push: {userIds: userId}, $inc: {nbUser: 1}})
        .then(
            () => {
                User.updateOne({_id: userId}, {roomId: roomId, points: 0})
                    .then((user) => {
                        res.status(201).json({
                            message: 'Room joined successfully!'
                        })
                    })
                    .catch((error) => {
                        res.status(400).json({
                            error: error
                        });
                    });
            })
        .catch(
            (error) => {
                res.status(400).json({
                    error: error
                });
            })
}

exports.joinRoom = (req, res, next) => {
    User.findOne({_id: req.params.id})
        .then((user) => {
            if (user.roomId === req.body.roomId) {
                res.status(400).json({
                    error: "User already in the room"
                });
            } else {
                Room.findOne({_id: req.body.roomId})
                    .then((room) => {
                        if (room.nbMax <= room.userIds.length + room.userTempoIds.length)
                            res.status(400).json({error: "Room is full"});
                        else {
                            if (user.roomId && user.roomId !== "") {
                                Room.updateOne({_id: user.roomId},
                                    {$pull: {userIds: req.params.id}, $inc: {nbUser: -1}})
                                    .then(() => {
                                        Room.findOne({_id: user.roomId})
                                            .then((room) => {
                                                if (room.userIds.length === 0 && room.userTempoIds.length === 0) {
                                                    Room.deleteOne({_id: user.roomId})
                                                        .then(() => {
                                                            User.updateOne({_id: req.params.id}, {roomId: ""}
                                                            ).then(
                                                                addUserToRoom(req.body.roomId, req.params.id, res)
                                                            ).catch((error) => {
                                                                    res.status(404).json({
                                                                        error: error
                                                                    });
                                                                }
                                                            );
                                                        })
                                                        .catch(
                                                            (error) => {
                                                                res.status(404).json({
                                                                    error: error
                                                                });
                                                            }
                                                        )
                                                } else {
                                                    User.updateOne({_id: req.params.id}, {roomId: ""}
                                                    ).then(
                                                        addUserToRoom(req.body.roomId, req.params.id, res)
                                                    ).catch((error) => {
                                                            res.status(404).json({
                                                                error: error
                                                            });
                                                        }
                                                    );
                                                }
                                            })
                                            .catch((error) => {
                                                    res.status(404).json({
                                                        error: error
                                                    });
                                                }
                                            )
                                    })
                                    .catch((error) => {
                                            res.status(404).json({
                                                error: error
                                            });
                                        }
                                    )
                            } else {
                                addUserToRoom(req.body.roomId, req.params.id, res)
                            }
                        }
                    })
                    .catch((error) => {
                        res.status(400).json({
                            error: error
                        });
                    })
            }
        })
        .catch((error) => {
            res.status(400).json({
                error: error
            });
        })
};

exports.leaveRoom = (req, res, next) => {
    User.findOne({_id: req.params.id})
        .then((usr) => {
            if (usr.roomId !== req.body.roomId) {
                res.status(400).json({
                    error: "User is not in the room"
                });
            } else {
                Room.updateOne({_id: req.body.roomId},
                    {$pull: {userIds: req.params.id}, $inc: {nbUser: -1}})
                    .then(() => {
                        Room.findOne({_id: req.body.roomId})
                            .then((room) => {
                                if (room.userIds.length === 0 && room.userTempoIds.length === 0) {
                                    Room.deleteOne({_id: req.body.roomId})
                                        .then(() => {
                                            User.updateOne({_id: req.params.id}, {roomId: ""}
                                            ).then(
                                                res.status(201).json({
                                                    message: 'Room left and deleted'
                                                })
                                            ).catch((error) => {
                                                    res.status(404).json({
                                                        error: error
                                                    });
                                                }
                                            );
                                        })
                                        .catch(
                                            (error) => {
                                                res.status(404).json({
                                                    error: error
                                                });
                                            }
                                        )
                                } else {
                                    User.updateOne({_id: req.params.id}, {roomId: ""}
                                    ).then(
                                        res.status(201).json({
                                            message: 'Room left successfully!'
                                        })
                                    ).catch((error) => {
                                            res.status(404).json({
                                                error: error
                                            });
                                        }
                                    );
                                }
                            })
                            .catch((error) => {
                                    res.status(404).json({
                                        error: error
                                    });
                                }
                            )
                    })
                    .catch((error) => {
                            res.status(404).json({
                                error: error
                            });
                        }
                    )
            }
        })
        .catch((error) => {
            res.status(404).json({
                error: error
            });
        })
}


exports.rmFriendReq = (req, res, next) => {
    User.updateOne({_id: req.params.id}, {$pull: {friendReqs: {friendId: req.body.friendId}}}
    ).then(
        () => {
            res.status(201).json({
                message: 'Friend request removed successfully!'
            });
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};

exports.rmFriend = (req, res, next) => {
    User.updateOne({_id: req.params.id}, {$pull: {friendsIds: req.body.friendId}}
    ).then(
        User.updateOne({_id: req.body.friendId}, {$pull: {friendsIds: req.params.id}}
        ).then(
            () => {
                res.status(201).json({
                    message: 'Friend removed successfully!'
                });
            }
        ).catch(
            (error) => {
                res.status(400).json({
                    error: error
                });
            }
        )
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};

exports.deleteUser = (req, res, next) => {
    User.deleteOne({_id: req.params.id}).then(
        () => {
            res.status(200).json({
                message: 'Deleted!'
            });
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};

exports.deleteAllUser = (req, res, next) => {
    User.remove().then(
        () => {
            res.status(200).json({
                message: 'Deleted!'
            });
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
}

exports.getAllUsers = (req, res, next) => {
    User.find().then(
        (users) => {
            res.status(200).json(users);
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};

exports.getRoomReqs = (req, res, next) => {
    User.findOne({
        _id: req.params.id
    }).then(
        (user) => {
            res.status(200).json(user.roomReqs);
        }
    ).catch(
        (error) => {
            res.status(404).json({
                error: error
            });
        }
    );
};

exports.addRoomReq = (req, res, next) => {
    const datetime = new Date();
    const roomReq = new RoomRequest({
        roomId: req.body.roomId,
        date: datetime
    })
    User.updateOne({_id: req.params.id}, {$push: {roomReqs: roomReq}}
    ).then(
        () => {
            res.status(201).json({
                message: 'Room request added successfully!',
                requestId: roomReq.id
            });
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};

exports.rmRoomReq = (req, res, next) => {
    User.updateOne({_id: req.params.id}, {$pull: {roomReqs: {roomId: req.body.roomId}}}
    ).then(
        () => {
            res.status(201).json({
                message: 'Room request removed successfully!'
            });
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};
