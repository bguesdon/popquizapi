const Room = require('../models/room');
const User = require('../models/user');
const UserTempo = require('../models/userTempo');
const Question = require('../models/question');
const Answer = require('../models/answer');

exports.createRoom = (req, res, next) => {
    const room = new Room({
        nbUser: 0,
        nbMax: req.body.nbMax,
        name: req.body.name,
        questionTime: req.body.questionTime,
        nbQuestion: req.body.nbQuestion,
        isPublic: req.body.isPublic,
        userIds: req.body.userIds,
        userTempoIds: req.body.userTempoIds,
        startGame: false,
        questions: [],
        answers: []
    });
    room.save().then(
        () => {
            res.status(201).json({
                message: 'Room saved successfully!',
                roomId: room._id
            });
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};

exports.getRoom = (req, res, next) => {
    Room.findOne({
        _id: req.params.id
    }).then(
        (room) => {
            res.status(200).json(room);
        }
    ).catch(
        (error) => {
            res.status(404).json({
                error: error
            });
        }
    );
};

exports.getPublicRooms = (req, res, next) => {
    Room.find({isPublic: true})
        .then(
            (room) => {
                res.status(200).json(room);
            }
        ).catch(
        (error) => {
            res.status(404).json({
                error: error
            });
        }
    );
}

exports.getPrivateRooms = (req, res, next) => {
    Room.find({isPublic: false})
        .then(
            (room) => {
                res.status(200).json(room);
            }
        ).catch(
        (error) => {
            res.status(404).json({
                error: error
            });
        }
    );
}

exports.addUser = (req, res, next) => {
    Room.updateOne({_id: req.params.id}, {$push: {userIds: req.body.userId}}).then(
        () => {
            res.status(201).json({
                message: 'User added successfully!'
            });
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};

exports.rmUser = (req, res, next) => {
    Room.updateOne({_id: req.params.id}, {$pull: {userIds: req.body.userId}}).then(
        () => {
            res.status(201).json({
                message: 'User removed successfully!'
            });
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};

exports.checkAnswer = (req, res, next) => {
    Question.findOne({_id: req.body.questionId})
        .then((quest) => {
            Answer.findOne({_id: quest.answerId})
                .then((answ) => {
                    let arr = answ.phrase.toLowerCase().split(' ');
                    let guess = req.body.answer.toLowerCase().split(' ');
                    if (answ.phrase.toLowerCase() === req.body.answer.toLowerCase()) {
                        User.updateOne({_id: req.body.userId}, {$inc: {points: req.body.time}})
                            .then(() => {
                                UserTempo.updateOne({_id: req.body.userId}, {$inc: {points: req.body.time}})
                                    .then(() => {
                                        res.status(201).json({
                                            message: 'Perfect answer !'
                                        });
                                    })
                            })
                            .catch((error) => {
                                res.status(400).json({error: "User not found"});
                            });
                    } else if (arr[0] === guess[0]) {
                        res.status(201).json({message: 'almost'});
                    } else {
                        res.status(201).json({message: 'false'});
                    }
                })
                .catch((error) => {
                    res.status(400).json({error: "Answer not found"});
                });
        })
        .catch((error) => {
            res.status(400).json({error: "Question not found"});
        });
}

exports.getAllPoints = (req, res, next) => {
    Room.findOne({_id: req.params.id})
        .then((room) => {
            User.find({_id: {$in: room.userIds}})
                .then((users) => {
                    UserTempo.find({_id: {$in: room.userTempoIds}})
                        .then((usersTempo) => {
                            res.status(201).json({users: users, usersTempo: usersTempo});
                        })
                })
                .catch((error) => {
                    res.status(400).json({error: "User not found."});
                });
        })
        .catch((error) => {
            res.status(400).json({error: "Room not found"});
        });
}

exports.startGame = (req, res, next) => {
    Room.findOne({_id: req.params.id})
        .then((room) => {
            User.updateMany({_id: {$in: room.userIds}}, {points: 0})
                .then(() => {
                    UserTempo.updateMany({_id: {$in: room.userTempoIds}}, {points: 0})
                        .then(() => {
                            Question.countDocuments()
                                .then((cnt) => {
                                    let arr = [];
                                    while (arr.length < room.nbQuestion && arr.length < cnt) {
                                        const r = Math.floor(Math.random() * cnt) + 1;
                                        if (arr.indexOf(r) === -1) arr.push(r);
                                    }
                                    Question.find({nb: {$in: arr}})
                                        .then((quest) => {
                                            Answer.find({nb: {$in: arr}})
                                                .then((answ) => {
                                                    Room.updateOne({_id: req.params.id}, {
                                                        startGame: true, questions: quest,
                                                        answers: answ
                                                    })
                                                        .then(
                                                            () => {
                                                                res.status(201).json({
                                                                    message: 'Game started succesfully!'
                                                                });
                                                            })
                                                        .catch(
                                                            (error) => {
                                                                res.status(400).json({
                                                                    error: error
                                                                });
                                                            }
                                                        );
                                                })
                                                .catch(
                                                    (error) => {
                                                        res.status(400).json({
                                                            error: error
                                                        });
                                                    }
                                                );
                                        })
                                        .catch(
                                            (error) => {
                                                res.status(400).json({
                                                    error: error
                                                });
                                            }
                                        );
                                })
                                .catch(
                                    (error) => {
                                        res.status(400).json({
                                            error: error
                                        });
                                    }
                                );
                        })
                })
        })
        .catch(
            (error) => {
                res.status(400).json({
                    error: error
                });
            }
        );
}

exports.stopGame = (req, res, next) => {
    Room.updateOne({_id: req.params.id}, {startGame: false, questions: [], answers: []})
        .then(
            () => {
                res.status(201).json({
                    message: 'Game stopped successfully!'
                });
            }
        ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
}


exports.modifyRoom = (req, res, next) => {
    Room.updateOne({_id: req.params.id}, {...req.body, _id: req.params.id})
        .then(
            () => {
                res.status(201).json({
                    message: 'Room updated successfully!'
                });
            })
        .catch(
            (error) => {
                res.status(400).json({
                    error: error
                });
            }
        );
};

exports.deleteRoom = (req, res, next) => {
    Room.findOne({_id: req.params.id}).then(
        (room) => {
            room.userIds.forEach((userId) => {
                User.updateOne({_id: userId}, {roomId: ""})
                    .catch(
                        (error) => {
                            res.status(400).json({
                                error: error
                            });
                        }
                    );
            })
            room.userTempoIds.forEach((userTempoId) => {
                UserTempo.updateOne({_id: userTempoId}, {roomId: ""})
                    .catch(
                        (error) => {
                            res.status(400).json({
                                error: error
                            });
                        }
                    );
            })
            Room.deleteOne({_id: req.params.id}).then(
                () => {
                    res.status(200).json({
                        message: 'Deleted!'
                    });
                }
            ).catch(
                (error) => {
                    res.status(400).json({
                        error: error
                    });
                }
            );
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};

exports.getAllRooms = (req, res, next) => {
    Room.find().then(
        (rooms) => {
            res.status(200).json(rooms);
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};

exports.getRoomInfos = (req, res, next) => {
    Room.findOne({_id: req.params.id}).then(
        (room) => {
            const infos = {
                roomName: room.name,
                nbQuestion: room.nbQuestion,
                questionTime: room.questionTime,
                nbMax: room.nbMax,
                nbUser: room.nbUser,
                users: [],
                usersTempo: [],
                startGame: room.startGame,
                questions: room.questions,
                answers: room.answers,
            };
            User.find({_id: {$in: room.userIds}})
                .then((user) => {
                    UserTempo.find({_id: {$in: room.userTempoIds}})
                        .then((userTmp) => {
                            infos.users = user;
                            infos.usersTempo = userTmp;
                            res.status(200).json(infos);
                        })
                        .catch((error) => {
                            res.status(404).json({error: error});
                        });
                })
                .catch((error) => {
                res.status(404).json({error: error});
            });
        }).catch((error) => {
            res.status(404).json({error: error});
        });
}

exports.deleteAllRooms = (req, res, next) => {
    Room.remove().then(
        () => {
            res.status(200).json({
                message: 'Deleted!'
            });
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
}
