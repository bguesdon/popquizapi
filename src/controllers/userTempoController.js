const UserTempo = require('../models/userTempo');
const Room = require('../models/room')

exports.createUserTempo = (req, res, next) => {
    const user = new UserTempo({
        name: req.body.name,
        roomId: req.body.roomId,
        points: 0,
    });
    user.save().then(
        () => {
            res.status(201).json({
                message: 'UserTempo created succesfully',
                userId: user._id
            });
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};

exports.getUserTempo = (req, res, next) => {
    UserTempo.findOne({
        _id: req.params.id
    }).then(
        (user) => {
            res.status(200).json(user);
        }
    ).catch(
        (error) => {
            res.status(404).json({
                error: error
            });
        }
    );
};

exports.getPoints = (req, res, next) => {
    UserTempo.findOne({
        _id: req.params.id
    }).then(
        (user) => {
            res.status(200).json(user.points);
        }
    ).catch(
        (error) => {
            res.status(404).json({
                error: error
            });
        }
    );
};

exports.modifyUserTempo = (req, res, next) => {
    UserTempo.updateOne({_id: req.params.id}, {...req.body, _id: req.params.id}).then(
        () => {
            res.status(201).json({
                message: 'UserTempo updated successfully!'
            });
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};

function addUserTempoToRoom(roomId, userId, res) {
    Room.updateOne({_id: roomId},
        {$push: {userTempoIds: userId}, $inc: {nbUser: 1}})
        .then(
            () => {
                UserTempo.updateOne({_id: userId}, {roomId: roomId, points: 0})
                    .then((user) => {
                        res.status(201).json({
                            message: 'Room joined successfully!'
                        })
                    })
                    .catch((error) => {
                        res.status(400).json({
                            error: error
                        });
                    });
            })
        .catch(
            (error) => {
                res.status(400).json({
                    error: error
                });
            })
}

exports.joinRoom = (req, res, next) => {
    UserTempo.findOne({_id: req.params.id})
        .then((user) => {
            if (user.roomId === req.body.roomId) {
                res.status(400).json({
                    error: "UserTempo already in the room"
                });
            } else {
                Room.findOne({_id: req.body.roomId})
                    .then((room) => {
                        if (room.nbMax <= room.userTempoIds.length + room.userIds.length)
                            res.status(400).json({error: "Room is full"});
                        else {
                            if (user.roomId && user.roomId !== "") {
                                Room.updateOne({_id: user.roomId},
                                    {$pull: {userTempoIds: req.params.id}, $inc: {nbUser: -1}})
                                    .then(() => {
                                        Room.findOne({_id: user.roomId})
                                            .then((room) => {
                                                if (room.userIds.length === 0 && room.userTempoIds.length === 0) {
                                                    Room.deleteOne({_id: user.roomId})
                                                        .then(() => {
                                                            UserTempo.updateOne({_id: req.params.id}, {roomId: ""}
                                                            ).then(
                                                                addUserTempoToRoom(req.body.roomId, req.params.id, res)
                                                            ).catch((error) => {
                                                                    res.status(404).json({
                                                                        error: error
                                                                    });
                                                                }
                                                            );
                                                        })
                                                        .catch(
                                                            (error) => {
                                                                res.status(404).json({
                                                                    error: error
                                                                });
                                                            }
                                                        )
                                                } else {
                                                    UserTempo.updateOne({_id: req.params.id}, {roomId: ""}
                                                    ).then(
                                                        addUserTempoToRoom(req.body.roomId, req.params.id, res)
                                                    ).catch((error) => {
                                                            res.status(404).json({
                                                                error: error
                                                            });
                                                        }
                                                    );
                                                }
                                            })
                                            .catch((error) => {
                                                    res.status(404).json({
                                                        error: error
                                                    });
                                                }
                                            )
                                    })
                                    .catch((error) => {
                                            res.status(404).json({
                                                error: error
                                            });
                                        }
                                    )
                            } else {
                                addUserTempoToRoom(req.body.roomId, req.params.id, res)
                            }
                        }
                    })
                    .catch((error) => {
                        res.status(400).json({
                            error: error
                        });
                    })
            }
        })
        .catch((error) => {
            res.status(400).json({
                error: error
            });
        })
};

exports.leaveRoom = (req, res, next) => {
    UserTempo.findOne({_id: req.params.id})
        .then((usr) => {
            if (usr.roomId !== req.body.roomId) {
                res.status(400).json({
                    error: "UserTempo is not in the room"
                });
            } else {
                Room.updateOne({_id: req.body.roomId},
                    {$pull: {userTempoIds: req.params.id}, $inc: {nbUser: -1}})
                    .then(() => {
                        Room.findOne({_id: req.body.roomId})
                            .then((room) => {
                                if (room.userTempoIds.length === 0 && room.userIds.length === 0) {
                                    Room.deleteOne({_id: req.body.roomId})
                                        .then(() => {
                                            UserTempo.updateOne({_id: req.params.id}, {roomId: ""}
                                            ).then(
                                                res.status(201).json({
                                                    message: 'Room left and deleted'
                                                })
                                            ).catch((error) => {
                                                    res.status(404).json({
                                                        error: error
                                                    });
                                                }
                                            );
                                        })
                                        .catch(
                                            (error) => {
                                                res.status(404).json({
                                                    error: error
                                                });
                                            }
                                        )
                                } else {
                                    UserTempo.updateOne({_id: req.params.id}, {roomId: ""}
                                    ).then(
                                        res.status(201).json({
                                            message: 'Room left successfully!'
                                        })
                                    ).catch((error) => {
                                            res.status(404).json({
                                                error: error
                                            });
                                        }
                                    );
                                }
                            })
                            .catch((error) => {
                                    res.status(404).json({
                                        error: error
                                    });
                                }
                            )
                    })
                    .catch((error) => {
                            res.status(404).json({
                                error: error
                            });
                        }
                    )
            }
        })
        .catch((error) => {
            res.status(404).json({
                error: error
            });
        })
}

exports.deleteUserTempo = (req, res, next) => {
    UserTempo.deleteOne({_id: req.params.id}).then(
        () => {
            res.status(200).json({
                message: 'Deleted!'
            });
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};

exports.deleteAllUserTempo = (req, res, next) => {
    UserTempo.remove().then(
        () => {
            res.status(200).json({
                message: 'Deleted!'
            });
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
}

exports.getAllUsersTempo = (req, res, next) => {
    UserTempo.find().then(
        (users) => {
            res.status(200).json(users);
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};
