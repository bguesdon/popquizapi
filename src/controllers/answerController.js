const Answer = require('../models/answer')

exports.getAnswer = (req, res, next) => {
    Answer.findOne({
        _id: req.params.id
    }).then(
        (answer) => {
            res.status(200).json(answer);
        }
    ).catch(
        (error) => {
            res.status(404).json({
                error: error
            });
        }
    );
};

exports.modifyAnswer = (req, res, next) => {
    Answer.updateOne({_id: req.params.id}, {...req.body, _id: req.params.id}).then(
        () => {
            res.status(201).json({
                message: 'Answer updated successfully!'
            });
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};

exports.getAllAnswers = (req, res, next) => {
    Answer.find().then(
        (answers) => {
            res.status(200).json(answers);
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};
