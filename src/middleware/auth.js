const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(' ')[1];
        jwt.verify(token, 'TOKEN_SECRET',(err, decoded) => {
            if (err || !decoded) {
                return res.status(401).json({message: 'Invalid token'})
            }
            if (req.params.id) {
                if (req.params.id !== decoded.userId) {
                    return res.status(401).json({message: 'Invalid token'})
                }
            }
            next()
        });
    } catch {
        res.status(401).json({
            error: 'Invalid request!'
        });
    }
};
