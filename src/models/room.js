const mongoose = require('mongoose');

const roomSchema = mongoose.Schema({
    nbUser: {type: Number, required: true},
    nbMax: {type: Number, required: true},
    name: { type: String, required: true },
    questionTime: { type: Number, required: true },
    nbQuestion: {type: Number, required: true},
    isPublic: {type: Boolean, required: true},
    userIds: {type: Array, required: false},
    userTempoIds: {type: Array, required: false},
    startGame: {type: Boolean, required: false},
    questions: {type: Array, required: false},
    answers: {type: Array, required: false}
});

module.exports = mongoose.model('Room', roomSchema);
