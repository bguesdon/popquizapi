const mongoose = require('mongoose');

const userTempoSchema = mongoose.Schema({
    name: { type: String, required: true },
    roomId: {type: String, required: false},
    points: {type: Number, required: false}
});

module.exports = mongoose.model('UserTempo', userTempoSchema);
