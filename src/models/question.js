const mongoose = require('mongoose');

const questionSchema = mongoose.Schema({
    phrase: {type: String, required: false},
    image: {type: String, required: false},
    answerId: {type: String, required: true},
    nb: {type: Number, required: true},
});

module.exports = mongoose.model('Question', questionSchema);
