const mongoose = require('mongoose');

const friendRequestSchema = mongoose.Schema({
    friendId: {type: String, required: true},
    date: {type: String, required: false}
});

module.exports = mongoose.model('FriendRequest', friendRequestSchema);
