const mongoose = require('mongoose');

const roomRequestSchema = mongoose.Schema({
    roomId: {type: String, required: true},
    date: {type: String, required: false}
});

module.exports = mongoose.model('RoomRequest', roomRequestSchema);
