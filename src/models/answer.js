const mongoose = require('mongoose');

const answerSchema = mongoose.Schema({
    phrase: {type: String, required: true},
    questionId: {type: String, required: true},
    nb: {type: Number, required: true},
});

module.exports = mongoose.model('Answer', answerSchema);
