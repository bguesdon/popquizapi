const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    name: { type: String, required: true },
    mail: { type: String, required: true },
    password: { type: String, required: true, select: false },
    friendsIds: {type: Array, required: false},
    friendReqs: {type: Array, required: false},
    roomReqs: {type: Array, required: false},
    roomId: {type: String, required: false},
    points: {type: Number, required: false}
});

module.exports = mongoose.model('User', userSchema);
