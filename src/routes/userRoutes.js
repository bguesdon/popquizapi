const express = require('express');
const router = express.Router();

const userCtrl = require('../controllers/userController');
const auth = require('../middleware/auth');

router.get('/', userCtrl.getAllUsers);
router.delete('/', userCtrl.deleteAllUser);
router.get('/checkToken', auth, userCtrl.checkToken);
router.get('/:id', userCtrl.getUser);
router.put('/:id', userCtrl.modifyUser);

router.delete('/:id', userCtrl.deleteUser);

router.get('/:id/getFriends', userCtrl.getFriends);
router.put('/:id/addFriend', userCtrl.addFriend);
router.put('/:id/rmFriend', userCtrl.rmFriend);

router.get('/:id/getFriendReqs', userCtrl.getFriendReqs);
router.put('/:id/addFriendReq', userCtrl.addFriendReq);
router.put('/:id/rmFriendReq', userCtrl.rmFriendReq);

router.put('/:id/joinRoom', userCtrl.joinRoom);
router.put('/:id/leaveRoom', userCtrl.leaveRoom);

router.get('/:id/getPoints', userCtrl.getPoints);

router.get('/:id/getRoomReqs', userCtrl.getRoomReqs);
router.put('/:id/addRoomReq', userCtrl.addRoomReq);
router.put('/:id/rmRoomReq', userCtrl.rmRoomReq);

module.exports = router;
