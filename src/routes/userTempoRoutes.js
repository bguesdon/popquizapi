const express = require('express');
const router = express.Router();

const userTempoCtrl = require('../controllers/userTempoController');

router.get('/', userTempoCtrl.getAllUsersTempo);
router.post('/', userTempoCtrl.createUserTempo);
router.delete('/', userTempoCtrl.deleteAllUserTempo);
router.get('/:id', userTempoCtrl.getAllUsersTempo);
router.put('/:id', userTempoCtrl.modifyUserTempo);
router.delete('/:id', userTempoCtrl.deleteUserTempo);

router.put('/:id/joinRoom', userTempoCtrl.joinRoom);
router.put('/:id/leaveRoom', userTempoCtrl.leaveRoom);

router.get('/:id/getPoints', userTempoCtrl.getPoints);

module.exports = router;
