const express = require('express');
const router = express.Router();

const authCtrl = require('../controllers/authController');

router.post('/login', authCtrl.login);
router.post('/register', authCtrl.register);
router.post('/googleCallback', authCtrl.googleCallback);
router.post('/facebookCallback', authCtrl.facebookCallback);


module.exports = router;
