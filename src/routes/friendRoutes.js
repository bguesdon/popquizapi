const express = require('express');
const router = express.Router();

const friendCtrl = require('../controllers/friendController');

router.get('/', friendCtrl.getAllFriends);
router.post('/', friendCtrl.createFriend);
router.delete('/', friendCtrl.deleteAllFriends);

router.get('/:id', friendCtrl.getFriend);
router.put('/:id', friendCtrl.modifyFriend);
router.delete('/:id', friendCtrl.deleteFriend);
router.get('/names', friendCtrl.getAllFriendsNames);

module.exports = router;
