const express = require('express');
const router = express.Router();

const questionCtrl = require('../controllers/questionController');

router.get('/', questionCtrl.getAllQuestions);
router.post('/', questionCtrl.createQuestion);
router.delete('/', questionCtrl.deleteAllQuestions);

router.get('/:id', questionCtrl.getQuestion);
router.put('/:id', questionCtrl.modifyQuestion);
router.delete('/:id', questionCtrl.deleteQuestion);

module.exports = router;
