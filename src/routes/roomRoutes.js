const express = require('express');
const router = express.Router();

const roomCtrl = require('../controllers/roomController');

router.get('/', roomCtrl.getAllRooms);
router.post('/', roomCtrl.createRoom);
router.delete('/', roomCtrl.deleteAllRooms);

router.put('/:id', roomCtrl.modifyRoom);
router.delete('/:id', roomCtrl.deleteRoom);
router.get('/getPublicRooms', roomCtrl.getPublicRooms);
router.get('/getPrivateRooms', roomCtrl.getPrivateRooms);

router.get('/:id', roomCtrl.getRoom);

router.put('/:id/addUser', roomCtrl.addUser);
router.put('/:id/rmUser', roomCtrl.rmUser);

router.put('/:id/startGame', roomCtrl.startGame);
router.put('/:id/stopGame', roomCtrl.stopGame);

router.get('/:id/getRoomInfos', roomCtrl.getRoomInfos);

router.post('/checkAnswer', roomCtrl.checkAnswer);
router.get('/:id/getAllPoints', roomCtrl.getAllPoints);

module.exports = router;
