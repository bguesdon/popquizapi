const express = require('express');
const router = express.Router();

const answerCtrl = require('../controllers/answerController');

router.get('/', answerCtrl.getAllAnswers);
router.get('/:id', answerCtrl.getAnswer);
router.put('/:id', answerCtrl.modifyAnswer);

module.exports = router;
